网站域名：
www.witprobe.com

根目录下主要文件介绍：
upload文件夹中是二次开发前的opencart
third_party文件夹中是第三方文件引用
redevelop文件夹是二次开发文件夹
部署：
将upload复制到apache下，改为opencart（可根据需要任意取名），再讲redevelop下的文件复制到刚才的opencart文件夹中。会有部分文件覆盖。
部署完成后需要安装，按照opencart原方式安装即可。
安装完成后，商品信息等均为示例，需要录入相应商品信息。