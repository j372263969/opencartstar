<?php
class crowdfunding_ModelCatalogProduct extends ModelCatalogProduct {
	public function addProduct($data) {
		$product_id = parent::addProduct($data);
		$this->db->query("
			INSERT INTO `". DB_PREFIX ."crowdfunding` (product_id) values ('". (int)$product_id ."');
		");
		return $product_id;
	}

	public function deleteProduct($product_id) {
		parent::deleteProduct($product_id);
		$this->db->query("DELETE FROM `".DB_PREFIX."crowdfunding WHERE product_id = '". (int)$product_id ."'");
	}
}
