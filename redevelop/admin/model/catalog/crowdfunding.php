<?php
class ModelCatalogCrowdfunding extends Model {
	
	public function getTotalProducts($data = array()) {
		$sql = "SELECT COUNT(DISTINCT p.product_id) AS total FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "crowdfunding cf ON (p.product_id = cf.product_id)";
		$sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		if (!empty($data['filter_name'])) {
			$sql .= " AND pd.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_model'])) {
			$sql .= " AND p.model LIKE '%" . $this->db->escape($data['filter_model']) . "%'";
		}

		if (!empty($data['filter_status'])) {
			$sql .= " AND cf.crowdfunding_status LIKE '" . $this->db->escape($data['filter_status']) . "'";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getProducts($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "crowdfunding cf ON (p.product_id = cf.product_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		if (!empty($data['filter_name'])) {
			$sql .= " AND pd.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_model'])) {
			$sql .= " AND p.model LIKE '%" . $this->db->escape($data['filter_model']) . "%'";
		}

		if (!empty($data['filter_status'])) {
			$sql .= " AND cf.crowdfunding_status LIKE '" . $this->db->escape($data['filter_status']) . "'";
		}

		$sql .= " GROUP BY p.product_id";

		$sort_data = array(
			'pd.name',
			'p.model',
			'p.price',
			'cf.accomplishment',
			'cf.target',
			'cf.crowdfunding_status',
			'p.sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY pd.name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getCrowdfunding($crowdfunding_id) {
		$query = $this->db->query("SELECT * from `".DB_PREFIX."crowdfunding` cf left join `".DB_PREFIX."product_description` pd on (cf.product_id = pd.product_id) where cf.crowdfunding_id = ".(int)$crowdfunding_id);
		return $query->row;
	}

	public function editCrowdfunding($crowdfunding_id, $data) {
		$sql = "update `".DB_PREFIX."crowdfunding` cf set";
		$sql .= " target = ".(int)$data['target'];
		$sql .= ",deadline = '".$data['deadline']."'";
		$sql .= ",crowdfunding_status = '".$data['crowdfunding_status']."'";
		$sql.=" where cf.crowdfunding_id = ".(int)$crowdfunding_id;
		
		error_log($sql,3,"e:/log.txt");

		$this->db->query($sql);
	}
}
?>