<?php
// Heading
$_['heading_title']          = 'Crowdfunding';

// Text
$_['text_success']           = 'Success: You have modified crowdfunding!';
$_['text_list']              = 'Crowdfunding List';
$_['text_add']               = 'Add Crowdfunding';
$_['text_edit']              = 'Edit Crowdfunding';
$_['text_default']           = 'Default';
$_['text_option']            = 'Option';
$_['text_option_value']      = 'Option Value';
$_['text_percent']           = 'Percentage';
$_['text_amount']            = 'Fixed Amount';
$_['text_crowdfunding_disable'] = 'Crowdfunding module is disable';
$_['text_null']              = '-';
$_['text_status_completed']  = 'Completed';
$_['text_status_disabled']    = 'Disabled';
$_['text_status_enabled']    = 'Enabled';
$_['text_status_incompleted']  = 'Incompleted';
$_['text_not_installed']     = 'This module is not installed.Please install first!';
$_['text_stage_completed'] ='Completed';
$_['text_stage_incompleted'] ='Incompleted';

// Column
$_['column_name']            = 'Product Name';
$_['column_model']           = 'Model';
$_['column_image']           = 'Image';
$_['column_price']           = 'Price';
$_['column_accomplishment']  = 'Accomplishment';
$_['column_target']          = 'Target';
$_['column_status']          = 'Status';
$_['column_deadline']           = 'Deadline';
$_['column_stage']           = 'Stage';
$_['column_action']          = 'Action';

// Entry
$_['entry_name']             = 'Product Name';
$_['entry_target']             = 'Target Capital';
$_['entry_deadline']             = 'Deadline';
$_['entry_model']            = 'Model';
$_['entry_status']           = 'Status';

// Button
$_['button_save'] = 'Save';
$_['button_cancel'] = 'Cancel';
$_['button_edit'] = 'Edit';
$_['button_filter'] = 'Filter';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify crowdfunding!';
$_['error_target']           = 'Error: Target Capital is wrong, please check.';
$_['error_deadline']         = 'Error: Deadline is wrong, please check.';
$_['error_status']           = 'Error: Status is wrong, please check.';
