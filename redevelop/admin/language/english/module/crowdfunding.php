<?php
/*module language*/

$_['heading_title'] = 'Crowdfunding';

$_['text_home'] = 'Home';
$_['text_module'] = 'Modules';
$_['text_success'] = 'Success: You have modified module Crowdfunding!';
$_['text_edit'] = 'Edit';
$_['text_enabled'] = 'Enable';
$_['text_disabled'] = 'Disabled';

$_['entry_status'] = 'Status';

$_['button_save'] = 'Save';
$_['button_cancel'] = 'Cancel';

$_['error_permission'] = 'Warning: You do not have permission to modify module Crowdfunding!';
?>