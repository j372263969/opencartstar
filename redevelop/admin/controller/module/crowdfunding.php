<?php
class ControllerModuleCrowdfunding extends Controller {
	private $error = array();

	public function index() {
		$this->language->load('module/crowdfunding');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$setting_data = $this->request->post;
			$setting_data['crowdfunding_install'] = 1;
			$this->model_setting_setting->editSetting('crowdfunding', $setting_data);		
					
			$this->session->data['success'] = $this->language->get('text_success');
						
			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_status'] = $this->language->get('entry_status');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		if (isset($this->request->post['crowdfunding_status'])) {
			$data['crowdfunding_status'] = $this->request->post['crowdfunding_status'];
		} else {
			$data['crowdfunding_status'] = $this->config->get('crowdfunding_status');
		}

		$data['action'] = $this->url->link('module/crowdfunding', 'token=' . $this->session->data['token'], 'SSL');
		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/crowdfunding.tpl', $data));
	}
	public function install() {
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX ."crowdfunding` (
		  `crowdfunding_id` INT NOT NULL AUTO_INCREMENT,
		  `accomplishment` DECIMAL(15,4) NOT NULL DEFAULT 0,
		  `target` DECIMAL(15,4) NOT NULL DEFAULT 0,
		  `deadline` DATE NOT NULL,
		  `product_id` INT NOT NULL,
		  `crowdfunding_status` VARCHAR(25) NOT NULL DEFAULT 'disabled',
		  PRIMARY KEY (`crowdfunding_id`)
		)");

		$this->db->query("
			INSERT INTO `". DB_PREFIX ."crowdfunding` (product_id) select product_id from `". DB_PREFIX ."product`
		");

		$this->db->query("
			update `".DB_PREFIX."crowdfunding` cf set accomplishment = (select SUM(total) from `".DB_PREFIX."order_product` o where o.product_id = cf.product_id)
		");

		$this->load->model("setting/setting");
		$setting_data = array();
		$setting_data['crowdfunding_install'] = 1;
		$this->model_setting_setting->editSetting('crowdfunding',$setting_data);
	}
	public function uninstall() {
		$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "crowdfunding`");
	}
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/crowdfunding')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		return !$this->error;
	}
}
