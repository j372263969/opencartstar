<?php
class ControllerCatalogCrowdfunding extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/crowdfunding');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/crowdfunding');

		if($this->isInstalled())
			$this->getList();
		else
			$this->showNotInstalled();
	}

	public function isInstalled() {
		$install = $this->config->get("crowdfunding_install");
		if(isset($install) && $install)
			return 1;
		else
			return 0;
	}

	public function showNotInstalled() {
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		$data['text_not_installed'] = $this->language->get('text_not_installed');
		
		$this->response->setOutput($this->load->view('catalog/crowdfunding_not_installed.tpl', $data));
	}

	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_model'])) {
			$filter_model = $this->request->get['filter_model'];
		} else {
			$filter_model = null;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'pd.name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/crowdfunding', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		$data['value_incompleted'] = 'incompleted';
		$data['value_completed'] = 'completed';
		$data['value_disabled'] = 'disabled';
		$data['value_enabled'] = 'enabled';

		$data['products'] = array();

		$filter_data = array(
			'filter_name'	  => $filter_name,
			'filter_model'	  => $filter_model,
			'filter_status'   => $filter_status,
			'sort'            => $sort,
			'order'           => $order,
			'start'           => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'           => $this->config->get('config_limit_admin')
		);

		$product_total = $this->model_catalog_crowdfunding->getTotalProducts($filter_data);

		$results = $this->model_catalog_crowdfunding->getProducts($filter_data);

		$this->load->model('tool/image');

		foreach ($results as $result) {
			if (is_file(DIR_IMAGE . $result['image'])) {
				$image = $this->model_tool_image->resize($result['image'], 40, 40);
			} else {
				$image = $this->model_tool_image->resize('no_image.png', 40, 40);
			}
			if($result['crowdfunding_status'] == $data['value_disabled']) {
				$data['products'][] = array(
					'product_id' => $result['product_id'],
					'image'      => $image,
					'name'       => $result['name'],
					'model'      => $result['model'],
					'price'      => $result['price'],
					'quantity'   => $result['quantity'],
					'crowdfunding_id' => $this->language->get('text_null'),
					'crowdfunding_status'     => $this->language->get('text_status_disabled'),
					'target' => $this->language->get('text_null'),
					'accomplishment' => $this->language->get('text_null'),
					'deadline' => $this->language->get('text_null'),
					'edit'       => $this->url->link('catalog/crowdfunding/edit', 'token=' . $this->session->data['token'] . '&crowdfunding_id=' . $result['crowdfunding_id'] . $url, 'SSL'),
					'stage' => $this->language->get('text_null')
				);

			} else {
				if((int)$result['accomplishment'] >= (int)$result['target'])
					$crowdfunding_stage = $this->language->get("text_stage_completed");
				else
					$crowdfunding_stage = $this->language->get("text_stage_incompleted");
				$data['products'][] = array(
					'product_id' => $result['product_id'],
					'image'      => $image,
					'name'       => $result['name'],
					'model'      => $result['model'],
					'price'      => $result['price'],
					'quantity'   => $result['quantity'],
					'crowdfunding_id' => $result['crowdfunding_id'],
					'crowdfunding_status'     => $this->language->get('text_status_enabled'),
					'target' => $result['target'],
					'accomplishment' => $result['accomplishment'],
					'deadline' => $result['deadline'],
					'stage' => $crowdfunding_stage,
					'edit'       => $this->url->link('catalog/crowdfunding/edit', 'token=' . $this->session->data['token'] . '&crowdfunding_id=' . $result['crowdfunding_id'] . $url, 'SSL')
				);
			}
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');
		$data['text_status_disabled'] = $this->language->get('text_status_disabled');
		$data['text_status_enabled'] = $this->language->get('text_status_enabled');
		$data['text_status_incompleted'] = $this->language->get('text_status_incompleted');
		$data['text_status_completed'] = $this->language->get('text_status_completed');

		$data['column_image'] = $this->language->get('column_image');
		$data['column_name'] = $this->language->get('column_name');
		$data['column_model'] = $this->language->get('column_model');
		$data['column_price'] = $this->language->get('column_price');
		$data['column_accomplishment'] = $this->language->get('column_accomplishment');
		$data['column_target'] = $this->language->get('column_target');
		$data['column_status'] = $this->language->get('column_status');
		$data['column_deadline'] = $this->language->get('column_deadline');
		$data['column_stage'] = $this->language->get('column_stage');
		$data['column_action'] = $this->language->get('column_action');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_model'] = $this->language->get('entry_model');
		$data['entry_status'] = $this->language->get('entry_status');

		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_filter'] = $this->language->get('button_filter');

		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/crowdfunding', 'token=' . $this->session->data['token'] . '&sort=pd.name' . $url, 'SSL');
		$data['sort_model'] = $this->url->link('catalog/crowdfunding', 'token=' . $this->session->data['token'] . '&sort=p.model' . $url, 'SSL');
		$data['sort_price'] = $this->url->link('catalog/crowdfunding', 'token=' . $this->session->data['token'] . '&sort=p.price' . $url, 'SSL');
		$data['sort_accomplishment'] = $this->url->link('catalog/crowdfunding', 'token=' . $this->session->data['token'] . '&sort=cf.accomplishment' . $url, 'SSL');
		$data['sort_status'] = $this->url->link('catalog/crowdfunding', 'token=' . $this->session->data['token'] . '&sort=cf.crowdfunding_status' . $url, 'SSL');
		$data['sort_target'] = $this->url->link('catalog/crowdfunding', 'token=' . $this->session->data['token'] . '&sort=cf.target' . $url, 'SSL');
		$data['sort_order'] = $this->url->link('catalog/crowdfunding', 'token=' . $this->session->data['token'] . '&sort=p.sort_order' . $url, 'SSL');

		$pagination = new Pagination();
		$pagination->total = $product_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/crowdfunding', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');


		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($product_total - $this->config->get('config_limit_admin'))) ? $product_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $product_total, ceil($product_total / $this->config->get('config_limit_admin')));

		$data['filter_name'] = $filter_name;
		$data['filter_model'] = $filter_model;
		$data['filter_status'] = $filter_status;

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/crowdfunding_list.tpl', $data));
	}



	public function edit() {
		$this->load->language('catalog/crowdfunding');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/crowdfunding');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$editData = array(
				'target' => $this->request->post['target'],
				'crowdfunding_status' => $this->request->post['status'],
				'deadline' => $this->request->post['deadline']
			);
			$this->model_catalog_crowdfunding->editCrowdfunding($this->request->get['crowdfunding_id'], $editData);

			$this->session->data['success'] = $this->language->get('text_success');
			
			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_model'])) {
				$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
			}	
			
			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}	

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/crowdfunding', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		if(isset($this->request->get['crowdfunding_id']))
			$this->getForm();
		else
			$this->getList();
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_status_enabled'] = $this->language->get('text_status_enabled');
		$data['text_status_disabled'] = $this->language->get('text_status_disabled');

		$data['value_disabled'] = 'disabled';
		$data['value_enabled'] = 'enabled';

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_target'] = $this->language->get('entry_target');
		$data['entry_deadline'] = $this->language->get('entry_deadline');

		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['target'])) {
			$data['error_target'] = $this->error['target'];
		} else {
			$data['error_target'] = '';
		}

		if (isset($this->error['deadline'])) {
			$data['error_deadline'] = $this->error['deadline'];
		} else {
			$data['error_deadline'] = '';
		}

		if (isset($this->error['status'])) {
			$data['error_status'] = $this->error['status'];
		} else {
			$data['error_status'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/crowdfunding', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		$data['action'] = $this->url->link('catalog/crowdfunding/edit', 'token=' . $this->session->data['token'] . '&crowdfunding_id=' . $this->request->get['crowdfunding_id'] . $url, 'SSL');

		$data['cancel'] = $this->url->link('catalog/crowdfunding', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$crowdfunding_value = $this->model_catalog_crowdfunding->getCrowdfunding($this->request->get['crowdfunding_id']);
		
		$data['crowdfunding_id'] = $crowdfunding_value['crowdfunding_id'];
		$data['product_name']    = "'".$crowdfunding_value['name']."'";
		$data['target'] = $crowdfunding_value['target'];
		$data['deadline'] = $crowdfunding_value['deadline'];
		$data['status'] = $crowdfunding_value['crowdfunding_status'];

		$data['value_disabled'] = 'disabled';
		$data['value_enabled'] = 'enabled';

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');


		$this->response->setOutput($this->load->view('catalog/crowdfunding_form.tpl', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/crowdfunding')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!is_numeric($this->request->post['target'])) {
			$this->error['target'] = $this->language->get('error_target');
		}

		if (strtotime($this->request->post['deadline']) == FALSE || strtotime($this->request->post['deadline']) == -1) {
			$this->error['deadline'] = $this->language->get('error_deadline');
		}

		if (($this->request->post['status'] != 'enabled') && ($this->request->post['status'] != 'disabled')) {
			$this->error['status'] = $this->language->get('error_status');
		}

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		return !$this->error;
	}
}