<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-crowdfunding" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
	    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
	      <button type="button" class="close" data-dismiss="alert">&times;</button>
	    </div>
	    <?php } ?>
	    <?php if ($error_target) { ?>
	    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_target; ?>
	      <button type="button" class="close" data-dismiss="alert">&times;</button>
	    </div>
	    <?php } ?>
	    <?php if ($error_deadline) { ?>
	    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_deadline; ?>
	      <button type="button" class="close" data-dismiss="alert">&times;</button>
	    </div>
	    <?php } ?>
	    <?php if ($error_status) { ?>
	    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_status; ?>
	      <button type="button" class="close" data-dismiss="alert">&times;</button>
	    </div>
	    <?php } ?>
	    <form action="<?php echo $action;?>" method="post" enctype="multipart/form-data" id="form-crowdfunding">
	      <div class="panel panel-default">
	        <div class="panel-heading">
	          <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_edit ." ". $product_name; ?></h3>
	        </div>
	        <div class="panel-body">
	          <div class="well">
	            <div class="row">
	            	<div class="form-group">
	                	<label class="control-label" for="input-target"><?php echo $entry_target; ?></label>
	               		<input type="text" name="target" value="<?php echo $target; ?>" placeholder="<?php echo $entry_target; ?>" id="input-target" class="form-control" />
	                </div>
	            </div>
	 			<div class="row">
	            	<div class="form-group">
	                	<label class="control-label" for="input-deadline"><?php echo $entry_deadline; ?></label>
	               		<input type="date" name="deadline" value="<?php echo $deadline; ?>" placeholder="<?php echo $entry_deadline; ?>" id="input-deadline" class="form-control" />
	                </div>
	            </div>
	 			<div class="row">
	            	<div class="form-group">
	                	<label class="control-label" for="input-status"><?php echo $entry_status; ?></label>
	               		<select name="status" id="input-status" class="form-control">
		                  <option value="*"></option>
		                  <?php if ($status == $value_disabled) { ?>
		                  <option value="<?php echo $value_disabled; ?>" selected="selected"><?php echo $text_status_disabled; ?></option>
		                  <?php } else { ?>
		                  <option value="<?php echo $value_disabled; ?>"><?php echo $text_status_disabled; ?></option>
		                  <?php } ?>
		                  <?php if ($status == $value_enabled) { ?>
		                  <option value="<?php echo $value_enabled; ?>" selected="selected"><?php echo $text_status_enabled; ?></option>
		                  <?php } else { ?>
		                  <option value="<?php echo $value_enabled; ?>"><?php echo $text_status_enabled; ?></option>
		                  <?php } ?>
	                	</select>
	                </div>
	            </div>
	          </div>
	        </div>
	      </div>
	    </form>
	</div>
  </div>
</div>
<?php echo $footer; ?>